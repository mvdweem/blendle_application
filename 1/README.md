Feature: Get pdf fils from duck duck go, extract the images from these files and convert these images to a box size (500px) and set them to black/white

  Scenario: Get 10 pdf files matching the tags "cat", "dog" and "tree" put the results in the aruba "temp" directory. # features/get_images_to_500_grey.feature:3
    Given An empty directory called "tmp/aruba/temp"                                                                  # features/step_definitions/application_steps.rb:1
    When I successfully run `fetch_relevant_images.rb 10 temp cat dog tree` for up to 300 seconds                     # aruba-0.13.0/lib/aruba/cucumber/command.rb:27
    Then The directory "tmp/aruba/temp" should contain 10 sub directories                                             # features/step_definitions/application_steps.rb:6

  Scenario: Get 3 pdf files matching the tags "pizza", "cookie" and "dough" put the results in the aruba "bakery" directory. # features/get_images_to_500_grey.feature:9
    Given An empty directory called "tmp/aruba/bakery"                                                                       # features/step_definitions/application_steps.rb:1
    When I successfully run `fetch_relevant_images.rb 3 bakery pizza cookie dough` for up to 300 seconds                     # aruba-0.13.0/lib/aruba/cucumber/command.rb:27
    Then The directory "tmp/aruba/bakery" should contain 3 sub directories                                                   # features/step_definitions/application_steps.rb:6

2 scenarios (2 passed)
6 steps (6 passed)
2m49.351s
