Feature: Get pdf fils from duck duck go, extract the images from these files and convert these images to a box size (500px) and set them to black/white

    Scenario: Get 10 pdf files matching the tags "cat", "dog" and "tree" put the results in the aruba "temp" directory.
        Given An empty directory called "tmp/aruba/temp"
        When I successfully run `fetch_relevant_images.rb 10 temp cat dog tree` for up to 300 seconds
        Then The directory "tmp/aruba/temp" should contain 10 sub directories


    Scenario: Get 3 pdf files matching the tags "pizza", "cookie" and "dough" put the results in the aruba "bakery" directory.
        Given An empty directory called "tmp/aruba/bakery"
        When I successfully run `fetch_relevant_images.rb 3 bakery pizza cookie dough` for up to 300 seconds
        Then The directory "tmp/aruba/bakery" should contain 3 sub directories
