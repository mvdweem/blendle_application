Given(/^An empty directory called "([^"]*)"$/) do |directory|
    `mkdir -m 755 -p #{directory}`
end


Then(/^The directory "([^"]*)" should contain (\d+) sub directories$/) do |directory, count|

    dir_count = Dir["#{directory}/*"].count;

    if dir_count.to_i != count.to_i
        raise "Count of directories (#{dir_count}) did not match the asked (#{count}) directories."
    end

end