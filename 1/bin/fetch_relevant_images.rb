#!/usr/bin/env ruby

require 'fileutils'

def make_dir(dir)
      FileUtils.mkdir_p(dir)
end

def get_links(url, limit)
    puts url
    content = `curl -v #{url}`
    return content.scan(/https?.*\.pdf/).uniq.first(limit);
end

#Basic setup
arguments     = ARGV;

# get output directory
output_dir = arguments[1];
files = arguments[0].to_i || 1;

#setup the search string
arguments.shift(2)
keywords = arguments.join("+");

url = "https://duckduckgo.com/html/?q=" + keywords + "+filetype%3Apdf"

links = get_links(url, files)
make_dir(output_dir);

links.each.with_index do |pdf, i|

    i = i.to_s
    folder = output_dir+'/'+i
    puts folder
    make_dir(folder)

    pdf_file =folder+ "/base.pdf"
    out_file = File.new(pdf_file, "w")
    out_file.puts(`curl -v #{pdf}`)
    out_file.close

    output = `
                pdfimages "#{pdf_file}" "#{folder}/" -png; \
                mogrify -resize 500x500\> -.t -set colorspace RGB -colorspace gray -format jpg "#{folder}/*.png"; \
                rm -- #{folder}/*.png; \ rm -- -.t
             `

end
