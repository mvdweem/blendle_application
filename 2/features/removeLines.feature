Feature: Strip last line of specific files

Scenario: unix_script.rb is started with a .txt file that contains a list of numbers
    Given a file named "a.txt" with:
    """
    0
    1
    2
    3
    4
    5
    6
    7
    8
    9
    """

    When I successfully run `removeLines.rb`

    Then the file "a.txt" should contain exactly:
    """
    0
    1
    2
    3
    4
    5
    6
    7
    8
    """


Scenario: The file includes letters instead of numbers, also starts with BZ_
    Given a file named "BZ_TEST.pdf" with:
    """
    a
    b
    c
    d
    e
    f
    g
    h
    i
    j
    """

    When I successfully run `removeLines.rb`

    Then the file "BZ_TEST.pdf" should contain exactly:
    """
    a
    b
    c
    d
    e
    f
    g
    h
    i
    """
