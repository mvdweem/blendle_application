Feature: Calculates the bytes from each file containing an 'e' in the file name in the current working directory

    Scenario: A simple text file called "something.txt"
    Given a file named "something.txt" with:
    """
    my simple text
    """
    When I successfully run `getBytes.rb`
    Then the stdout should contain exactly "14"


Scenario: two simple text files

    Given a file named "something.txt" with:
    """
    my simple text
    """

    Given a file named "something_with_another-name.txt" with:
    """
    my simple text.
    """

    When I successfully run `getBytes.rb`

    Then the stdout should contain exactly "29"