#!/usr/bin/env ruby

bytes = 0;
dir = ARGV[0] || './'
Dir.glob("#{dir}/*e*.*").map do |filename|
    File.open(filename, "r") { |file| bytes += file.size } #read byte stream, convert into int size
end
$stdout.print bytes
