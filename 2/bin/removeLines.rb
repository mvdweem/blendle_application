#!/usr/bin/env ruby

Dir.glob(["**/*.txt","**/BZ_*.*"]).map do |filename|
    content = File.readlines(filename)[0..-2].join;
    File.open(filename, "w") { |file| file << content }
end
