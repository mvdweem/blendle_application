Feature: Calculates the bytes from each file containing an 'e' in the file name in the current working directory

  Scenario: A simple text file called "something.txt" # features/getBytes.feature:3
    Given a file named "something.txt" with:          # aruba-0.13.0/lib/aruba/cucumber/file.rb:23
      """
      my simple text
      """
    When I successfully run `getBytes.rb`             # aruba-0.13.0/lib/aruba/cucumber/command.rb:27
    Then the stdout should contain exactly "14"       # aruba-0.13.0/lib/aruba/cucumber/command.rb:159

  Scenario: two simple text files                              # features/getBytes.feature:12
    Given a file named "something.txt" with:                   # aruba-0.13.0/lib/aruba/cucumber/file.rb:23
      """
      my simple text
      """
    Given a file named "something_with_another-name.txt" with: # aruba-0.13.0/lib/aruba/cucumber/file.rb:23
      """
      my simple text.
      """
    When I successfully run `getBytes.rb`                      # aruba-0.13.0/lib/aruba/cucumber/command.rb:27
    Then the stdout should contain exactly "29"                # aruba-0.13.0/lib/aruba/cucumber/command.rb:159

Feature: Strip last line of specific files

  Scenario: unix_script.rb is started with a .txt file that contains a list of numbers # features/removeLines.feature:3
    Given a file named "a.txt" with:                                                   # aruba-0.13.0/lib/aruba/cucumber/file.rb:23
      """
      0
      1
      2
      3
      4
      5
      6
      7
      8
      9
      """
    When I successfully run `removeLines.rb`                                           # aruba-0.13.0/lib/aruba/cucumber/command.rb:27
    Then the file "a.txt" should contain exactly:                                      # aruba-0.13.0/lib/aruba/cucumber/file.rb:151
      """
      0
      1
      2
      3
      4
      5
      6
      7
      8
      """

  Scenario: The file includes letters instead of numbers, also starts with BZ_ # features/removeLines.feature:34
    Given a file named "BZ_TEST.pdf" with:                                     # aruba-0.13.0/lib/aruba/cucumber/file.rb:23
      """
      a
      b
      c
      d
      e
      f
      g
      h
      i
      j
      """
    When I successfully run `removeLines.rb`                                   # aruba-0.13.0/lib/aruba/cucumber/command.rb:27
    Then the file "BZ_TEST.pdf" should contain exactly:                        # aruba-0.13.0/lib/aruba/cucumber/file.rb:151
      """
      a
      b
      c
      d
      e
      f
      g
      h
      i
      """

4 scenarios (4 passed)
13 steps (13 passed)
0m0.535s
