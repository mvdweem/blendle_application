## Blendle application
### Matti van de Weem

#### products

| # |  task  |  time estimated  |
|:-:|---|---|
| 1/2  | Simple terminal applications  | 4 hours  |
|  3  |  HAL+JSON API + Client  |  25 hours |



`they won't match the current version`


| # | url |
|:-:|---|
| api server | http://readlr-api.herokuapp.com/api |
| api client | http://readlr.herokuapp.com |


## Getting started
Every folder contains it's own challenge, challenge 3 is split into a client and a server.
All tests are written in Cucumber, to test a challengy simple start with cucumber (1,2 and 3-server have tests included)


### `Fetch relevant images`
To be able to run task 1 you need `poppler` and `imagick` installed on your system.

### `getBytes & removeLines`
run the scripts from the bin, or simply test it with cucumber.

### `Readlr client+server`
- Runnin the server requires a mongodb server, and `bundle install`, you can edit the mongo settings inside the `server.rb`, you can start the server with `rackup [--setup]`

- Before running the client, make shure the api root is set inside the `app.rb` file, this should match your server, simple `bundle install` should be enough to get started, to start the server you should do a simple `ruby app.rb`


## Test
Each product with tests contains a test output file(should be the `README.md`, if not it's located in `test_output.txt`), this output is the plain output from `cucumber >> test_output.txt`


#### things i should improve:
 - Namespaces
 - Setup rack for the client
 - Some documentation on the client/server
 - setup bundles
 - ...
 - use the testing frameworks error handling (expect, assert etc) instead of raise



 Matti van de Weem | mattivandeweem.nl