D, [2016-02-26T18:20:17.779419 #31568] DEBUG -- : MONGODB | Adding home.dev:27017 to the cluster.
Feature: Users should be able to login and send requests as logged in users.

  Background:                                                                                                                                                                                                                                                                      # features/authorized_get.feature:3
    Given the clients wants to login with the session token "CAAEUz7bqxJ4BAOBOvWFmoAIZAhnVlFvnDZCid6KOrstqotDcJip4DyOdDCyvtQFEJ2yCL95iS4eOdG189HHWHAMqmTDOZBZBsXyqPZBF2JgUYglrDXbl67V2t94EDbZCItG7FfuggG7G2cZApmkxqicbQQiRzM8TuwNcOQ9ae2BhdOmkWdHaVya0EbFgFZCiJnkXVz3CKBi7aAZDZD " # features/step_definitions/apllicationsteps.rb:34
D, [2016-02-26T18:20:23.019795 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.find | STARTED | {"find"=>"sessions", "filter"=>{"auth_token"=>"CAAEUz7bqxJ4BAOBOvWFmoAIZAhnVlFvnDZCid6KOrstqotDcJip4DyOdDCyvtQFEJ2yCL95iS4eOdG189HHWHAMqmTDOZBZBsXyqPZBF2JgUYglrDXbl67V2t94EDbZCItG7FfuggG7G2cZApmkxqicbQQiRzM8TuwNcOQ9ae2BhdOmkWdHaVya0EbFgFZCiJnkXVz3CKBi...
D, [2016-02-26T18:20:23.020687 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.find | SUCCEEDED | 0.000818s
D, [2016-02-26T18:20:23.021272 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.find | STARTED | {"find"=>"users", "filter"=>{"id"=>"100001025355685"}}
D, [2016-02-26T18:20:23.021596 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.find | SUCCEEDED | 0.000281s

  Scenario: I want to get my user details            # features/authorized_get.feature:6
    When I make an authorized get request to "/user" # features/step_definitions/apllicationsteps.rb:38
    Then the response status should be 200           # features/step_definitions/apllicationsteps.rb:15
    Then the response should be:                     # features/step_definitions/apllicationsteps.rb:7
      """
      {
          "name": "Matti Van de Weem",
          "id": "100001025355685",
          "_id": {
              "$oid": "56bd89760ab475085efac9e9"
          },
          "money": 2316,
          "_links": {
              "self": {
                  "href": "/article/100001025355685"
              }
          }
      }
      """

D, [2016-02-26T18:20:23.053668 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.find | STARTED | {"find"=>"sessions", "filter"=>{"auth_token"=>"CAAEUz7bqxJ4BAOBOvWFmoAIZAhnVlFvnDZCid6KOrstqotDcJip4DyOdDCyvtQFEJ2yCL95iS4eOdG189HHWHAMqmTDOZBZBsXyqPZBF2JgUYglrDXbl67V2t94EDbZCItG7FfuggG7G2cZApmkxqicbQQiRzM8TuwNcOQ9ae2BhdOmkWdHaVya0EbFgFZCiJnkXVz3CKBi...
D, [2016-02-26T18:20:23.054177 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.find | SUCCEEDED | 0.000446s
D, [2016-02-26T18:20:23.054667 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.find | STARTED | {"find"=>"purchases", "filter"=>{"user_id"=>"100001025355685"}}
D, [2016-02-26T18:20:23.055064 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.find | SUCCEEDED | 0.00034199999999999996s
#<Purchase:0x007ff533f42178>
#<Purchase:0x007ff533c56ab8>
#<Purchase:0x007ff535ad1128>
  Scenario: I want to see a list of all my purchases      # features/authorized_get.feature:26
    When I make an authorized get request to "/purchases" # features/step_definitions/apllicationsteps.rb:38
    Then the response status should be 200                # features/step_definitions/apllicationsteps.rb:15
    Then the response should be:                          # features/step_definitions/apllicationsteps.rb:7
      """
      {
      	"_embedded": {
      		"purchases": [{
      			"name": "Odio occaecati ex et qui voluptas.",
      			"article_id": "56bd896c0ab475085efac9d9",
      			"price": 67
      		}, {
      			"name": "Accusamus aliquid quia eius neque vel et eum quae.",
      			"article_id": "56bd896c0ab475085efac9da",
      			"price": 41
      		}, {
      			"name": "Impedit quisquam non eligendi nesciunt.",
      			"article_id": "56bd896c0ab475085efac9db",
      			"price": 76
      		}]
      	},
      	"_links": {
      		"self": {
      			"href": "/purchases"
      		}
      	}
      }
      """

Feature: Users should be able to post a reaction (/make authorized post requests)

  Background:                                                                                                                                                                                                                                                                      # features/post_reaction.feature:3
    Given the clients wants to login with the session token "CAAEUz7bqxJ4BAOBOvWFmoAIZAhnVlFvnDZCid6KOrstqotDcJip4DyOdDCyvtQFEJ2yCL95iS4eOdG189HHWHAMqmTDOZBZBsXyqPZBF2JgUYglrDXbl67V2t94EDbZCItG7FfuggG7G2cZApmkxqicbQQiRzM8TuwNcOQ9ae2BhdOmkWdHaVya0EbFgFZCiJnkXVz3CKBi7aAZDZD " # features/step_definitions/apllicationsteps.rb:34
D, [2016-02-26T18:20:23.061533 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.find | STARTED | {"find"=>"sessions", "filter"=>{"auth_token"=>"CAAEUz7bqxJ4BAOBOvWFmoAIZAhnVlFvnDZCid6KOrstqotDcJip4DyOdDCyvtQFEJ2yCL95iS4eOdG189HHWHAMqmTDOZBZBsXyqPZBF2JgUYglrDXbl67V2t94EDbZCItG7FfuggG7G2cZApmkxqicbQQiRzM8TuwNcOQ9ae2BhdOmkWdHaVya0EbFgFZCiJnkXVz3CKBi...
D, [2016-02-26T18:20:23.061927 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.find | SUCCEEDED | 0.000343s
D, [2016-02-26T18:20:23.062366 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.find | STARTED | {"find"=>"users", "filter"=>{"id"=>"100001025355685"}}
D, [2016-02-26T18:20:23.062655 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.find | SUCCEEDED | 0.000247s
D, [2016-02-26T18:20:23.062874 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.insert | STARTED | {"insert"=>"reactions", "documents"=>[{:user=>{"_id"=>BSON::ObjectId('56bd89760ab475085efac9e9'), "id"=>"100001025355685", "name"=>"Matti Van de Weem", "money"=>2316}, :article_id=>"56bd896c0ab475085efac9d9", :content=>"this is my reaction", :_id=>BSO...
D, [2016-02-26T18:20:23.063231 #31568] DEBUG -- : MONGODB | home.dev:27017 | blendle_application.insert | SUCCEEDED | 0.000319s
  {
    "article_id": "56bd896c0ab475085efac9d9",
    "content": "this is my reaction"
  }

  Scenario: I want to be able to post a new reaction below the article "56bd896c0ab475085efac9d9" # features/post_reaction.feature:8
    When I make an authorized post request to "/reactions/56bd896c0ab475085efac9d9" with:         # features/step_definitions/apllicationsteps.rb:43
      
      """
        {
          "article_id": "56bd896c0ab475085efac9d9",
          "content": "this is my reaction"
        }
      """
    Then the response status should be 201                                                        # features/step_definitions/apllicationsteps.rb:15

Feature: root.json

  Scenario: get root.json                                                            # features/root.feature:3
    When I do a get request to "http://example.com/api"                              # features/step_definitions/apllicationsteps.rb:3
    Then the response status should be 200 with a content type of "application/json" # features/step_definitions/apllicationsteps.rb:21
    Then the response should be:                                                     # features/step_definitions/apllicationsteps.rb:7
      """
      {
         "_links":{
            "self":{
               "href":"/root"
            },
            "user":{
               "href":"/user"
            },
            "articles":{
               "href":"/articles"
            },
            "article":{
               "href":"/article/{id}",
               "templated":true
            },
            "purchases":{
               "href":"/purchases"
            },
            "purchase":{
               "href":"/purchase/{id}",
               "templated":true
            },
            "reactions":{
               "href":"/reactions/{id}",
               "templated":true
            }
         }
      }
      """

Feature: Authorization

  Scenario: Try to get the user object while you're not logged in # features/unauthorized.feature:3
    When I do a get request to "http://example.com/user"          # features/step_definitions/apllicationsteps.rb:3
    Then the response status should be 401                        # features/step_definitions/apllicationsteps.rb:15

  Scenario: Try to get the purchases object while you're not logged in # features/unauthorized.feature:7
    When I do a get request to "http://example.com/purchases"          # features/step_definitions/apllicationsteps.rb:3
    Then the response status should be 401                             # features/step_definitions/apllicationsteps.rb:15

  Scenario: Try to get an reaction while you're not logged in   # features/unauthorized.feature:11
    When I do a get request to "http://example.com/reactions/3" # features/step_definitions/apllicationsteps.rb:3
    Then the response status should be 401                      # features/step_definitions/apllicationsteps.rb:15

7 scenarios (7 passed)
20 steps (20 passed)
0m0.118s
