Feature: Users should be able to login and send requests as logged in users.

  Background:
      Given the clients wants to login with the session token "CAAEUz7bqxJ4BAOBOvWFmoAIZAhnVlFvnDZCid6KOrstqotDcJip4DyOdDCyvtQFEJ2yCL95iS4eOdG189HHWHAMqmTDOZBZBsXyqPZBF2JgUYglrDXbl67V2t94EDbZCItG7FfuggG7G2cZApmkxqicbQQiRzM8TuwNcOQ9ae2BhdOmkWdHaVya0EbFgFZCiJnkXVz3CKBi7aAZDZD "

  Scenario: I want to get my user details
      When I make an authorized get request to "/user"
      Then the response status should be 200
      Then the response should be:
      """json
      {
          "name": "Matti Van de Weem",
          "id": "100001025355685",
          "_id": {
              "$oid": "56bd89760ab475085efac9e9"
          },
          "money": 2316,
          "_links": {
              "self": {
                  "href": "/article/100001025355685"
              }
          }
      }
      """

Scenario: I want to see a list of all my purchases
      When I make an authorized get request to "/purchases"
      Then the response status should be 200
      Then the response should be:
      """json
      {
      	"_embedded": {
      		"purchases": [{
      			"name": "Odio occaecati ex et qui voluptas.",
      			"article_id": "56bd896c0ab475085efac9d9",
      			"price": 67
      		}, {
      			"name": "Accusamus aliquid quia eius neque vel et eum quae.",
      			"article_id": "56bd896c0ab475085efac9da",
      			"price": 41
      		}, {
      			"name": "Impedit quisquam non eligendi nesciunt.",
      			"article_id": "56bd896c0ab475085efac9db",
      			"price": 76
      		}]
      	},
      	"_links": {
      		"self": {
      			"href": "/purchases"
      		}
      	}
      }
      """
