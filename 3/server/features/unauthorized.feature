Feature: Authorization

    Scenario: Try to get the user object while you're not logged in
        When I do a get request to "http://example.com/user"
        Then the response status should be 401

    Scenario: Try to get the purchases object while you're not logged in
        When I do a get request to "http://example.com/purchases"
        Then the response status should be 401

    Scenario: Try to get an reaction while you're not logged in
        When I do a get request to "http://example.com/reactions/3"
        Then the response status should be 401