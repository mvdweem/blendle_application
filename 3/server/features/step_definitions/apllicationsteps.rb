require 'rack/utils'

When(/^I do a get request to "([^"]*)"$/) do |url|
  get url
end

Then(/^the response should be:$/) do |json|

  if !match_json(json, last_response.body)
    raise "response body did not match"
  end
end


Then(/^the response status should be (\d+)$/) do |status|
    if status.to_i != last_response.status.to_i
    raise "The status should be #{status}, retrieved: #{last_response.status}"
  end
end

Then(/^the response status should be (\d+) with a content type of "([^"]*)"$/) do |status, content_type|

  if content_type != last_response.header['Content-Type']
    raise "Content type should be #{content_type}, retrieved: #{last_response.header['Content-Type']}"
  end

  if status.to_i != last_response.status.to_i
    raise "The status should be #{status}, retrieved: #{last_response.status}"
  end

end


Given(/^the clients wants to login with the session token "([^"]*)"$/) do |token|
  Authtoken = token
end

When(/^I make an authorized get request to "([^"]*)"$/) do |url|
  header 'Authorization', Authtoken
  get url, {}, { 'Authorization' => Authtoken }
end

When(/^I make an authorized post request to "([^"]*)" with:$/) do |url, data|
  header 'Authorization', Authtoken
  post(url, "#{data}", { "CONTENT_TYPE" => "application/json" })
  puts last_response.body
end


## matches 2 json strings
def match_json(first, second)
  return JSON.parse("#{second}") == JSON.parse("#{first}")
end


