Feature: root.json

    Scenario: get root.json
        When I do a get request to "http://example.com/api"
        Then the response status should be 200 with a content type of "application/json"
        Then the response should be:
          """json
          {
             "_links":{
                "self":{
                   "href":"/root"
                },
                "user":{
                   "href":"/user"
                },
                "articles":{
                   "href":"/articles"
                },
                "article":{
                   "href":"/article/{id}",
                   "templated":true
                },
                "purchases":{
                   "href":"/purchases"
                },
                "purchase":{
                   "href":"/purchase/{id}",
                   "templated":true
                },
                "reactions":{
                   "href":"/reactions/{id}",
                   "templated":true
                }
             }
          }
          """
