require 'webmachine/adapters/rack'
require 'rack/test'
require 'rack/utils'

require_relative '../../server'

def app
    App.adapter
end

World(Rack::Test::Methods)
