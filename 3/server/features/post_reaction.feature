Feature: Users should be able to post a reaction (/make authorized post requests)

  Background:
      Given the clients wants to login with the session token "CAAEUz7bqxJ4BAOBOvWFmoAIZAhnVlFvnDZCid6KOrstqotDcJip4DyOdDCyvtQFEJ2yCL95iS4eOdG189HHWHAMqmTDOZBZBsXyqPZBF2JgUYglrDXbl67V2t94EDbZCItG7FfuggG7G2cZApmkxqicbQQiRzM8TuwNcOQ9ae2BhdOmkWdHaVya0EbFgFZCiJnkXVz3CKBi7aAZDZD "



  Scenario: I want to be able to post a new reaction below the article "56bd896c0ab475085efac9d9"
      When I make an authorized post request to "/reactions/56bd896c0ab475085efac9d9" with:
      """json
        {
          "article_id": "56bd896c0ab475085efac9d9",
          "content": "this is my reaction"
        }
      """
      Then the response status should be 201