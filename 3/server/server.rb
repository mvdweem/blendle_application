# bootstrap file: require, run repeat

require 'bundler/setup'
require 'webmachine'
require 'bson'
require 'mongo'
require 'json'
require 'roar/decorator'
require 'roar/json/hal'
require "http"

# today, today is not a day for SQL
DB = Mongo::Client.new(ENV['MONGOLAB_URI'] || 'mongodb://home.dev:27017/blendle_application')


if ARGV.shift == '--setup'
    load('db/setup.rb')
end

# call all resources, helpers models and routes
Dir.glob('app/{models,representers,helpers,controllers}/*.rb').each { |file|
    load(file)
}


App = Webmachine::Application.new do |app|
    app.routes do
      add ["articles"], ArticleResource
      add ["article", :id], ArticleResource
      add ["purchases"], PurchaseResource
      add ["purchase", :id], PurchaseResource
      add ["reactions", :id], ReactionResource
      add ["reactions"], ReactionResource
      add ["user"], UserResource
      add ["api"], RootResource
    end

    app.configure do |config|
          config.port = ENV['PORT'] || 8080
        config.adapter = :Rack
    end

end

#Webmachine.application.configure do |config|
#  config.port = ENV['PORT'] || 8080
#end

# Start a web server to serve requests via localhost
#Webmachine.application.run



