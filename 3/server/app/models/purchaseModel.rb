require_relative '_BaseModel.rb'
module PurchaseRepresenter
      include Roar::JSON::HAL

    attr_accessor :name, :_id, :id, :price, :article_id, :user_id
    property :name
    property :_id
    property :user_id
    property :id
    property :price
    property :article_id

    link :self do
        "/purchase/#{self.id}"
    end

end

class Purchase < BaseModel

end
