require_relative '_BaseModel.rb'

module RootRepresenter
      include Roar::JSON::HAL

    link :self do
        "/root"
    end

    link :user do
        "/user"
    end

    link :articles do
        "/articles"
    end
    link :article do
        {
              href: "/article/{id}",
            templated: true
        }
    end

    link :purchases do
        "/purchases"
    end

    link :purchase do
        {
              href: "/purchase/{id}",
            templated: true
        }
    end

    link :reactions do
        "/reactions"
    end

    link :reactions do
        {
              href: "/reactions/{id}",
            templated: true
        }
    end

end


class Root < BaseModel

end

