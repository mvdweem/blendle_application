require_relative '_BaseModel.rb'

module UserRepresenter
      include Roar::JSON::HAL

    attr_accessor :name, :_id, :id, :money
    property :name
    property :id
    property :_id
    property :money


    link :self do
        "/article/#{self.id}"
    end

end

class User < BaseModel

end
