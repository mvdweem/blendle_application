require_relative('articleModel.rb')

class Articles < BaseModel
    attr_accessor :articles
    property :articles

    collection :articles, extend: ArticleRepresenter, class: Article

end

module ArticlesRepresenter
    include Roar::JSON::HAL

    collection :articles, class: Article, extend: ArticleRepresenter, embedded: true

    link :self do
        "/articles"
    end
end
