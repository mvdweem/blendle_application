require_relative('purchaseModel.rb')

class Purchases < BaseModel

end

module PurchasesRepresenter
    include Roar::JSON::HAL

    attr_accessor :purchases

     collection :purchases, class: Purchase,  extend: PurchasesRepresenter, embedded: true do
        property :name
        property :article_id
        property :price
      end
    link :self do
        "/purchases"
    end
end

