require_relative '_BaseModel.rb'
module ArticleRepresenter
      include Roar::JSON::HAL

    attr_accessor :name, :_id, :id, :price, :content, :is_bought, :content_sample
    property :name
    property :_id
    property :id
    property :price
    property :content
    property :content_sample
    property :is_bought


    link :self do
        "/article/#{self.id}"
    end

end

class Article < BaseModel

end
