require_relative '_BaseModel.rb'
module ReactionRepresenter
      include Roar::JSON::HAL

    attr_accessor :_id, :id, :content, :user, :article_id
    property :_id
    property :id
    property :user
    property :content
    property :article_id


    link :self do
        "/reaction/#{self.id}"
    end

end

class Reaction < BaseModel

end
