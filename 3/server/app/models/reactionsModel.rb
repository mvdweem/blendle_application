require_relative('reactionModel.rb')



class Reactions < BaseModel

    attr_accessor :reactions
    property :reactions


    collection :reactions, extend: ReactionRepresenter, class: Reaction



end

module ReactionsRepresenter
      include Roar::JSON::HAL

     collection :reactions, class: Reaction, extend: ReactionRepresenter, embedded: true

    link :self do
        "/reactions"
    end
end