require_relative '_authController.rb'
class ReactionResource < AuthController
    def allowed_methods
#        if request.path_info[:id]
              %w[GET HEAD POST]
#        else
#            %w[POST]
#        end



    end



    def resource_exists?
        true
    end

    def allow_missing_post?
        true
    end

    def post_is_create?
        true
    end

    def create_path

        body =  JSON.parse(request.body.to_s)

        article_id = body['article_id']
        content = body['content']

        user = DB[:users].find({:id => @user_id}).to_a.first
        insert = DB[:reactions].insert_one({:user => user, :article_id => article_id, :content => content})

        @product = "/article/#{article_id}"

    end

    def content_types_provided
        [["application/json", :to_json]]
    end

    def content_types_accepted
        [["application/json", :from_json]]
    end

    def from_json
        puts request.body.to_s
    end

    def to_json
        prod = DB[:reactions].find({:article_id => request.path_info[:id]}).to_a
        products = []
        prod.each { |product|
            product[:id] = product[:_id].to_s
            products << Reaction.new.extend(ReactionRepresenter).from_json(product.to_json)
        }
        list = Reactions.new.extend(ReactionsRepresenter)
        list.reactions = products
        @product = list

        @product.to_json
    end
end