require_relative '_authController.rb'
class PurchaseResource < AuthController
    def allowed_methods
#        if request.path_info[:id]
              %w[GET HEAD POST]
#        else
#            %w[POST]
#        end



    end



    def resource_exists?
        true
    end

    def allow_missing_post?
        true
    end

    def post_is_create?
        true
    end

    def create_path

        puts "test"

        body =  JSON.parse(request.body.to_s)

        article_id = body['article_id']

        puts "bullcrap"
        article = DB[:articles].find(:_id => BSON::ObjectId(article_id)).to_a.first


        puts @user_id
        user = DB[:users].find({:id => @user_id})
        user_data = user.to_a.first
        money = (user_data[:money] - article[:price])
        puts money
        if money > 0
            product = DB[:purchases].insert_one({
                :name => article[:name],
                :price => article[:price],
                :article_id => article_id,
                :user_id => @user_id
            }).to_a.first

            user = DB[:users].find({:id => @user_id}).update_one("$set" => { :money => money })
            @product = "/article/#{article_id}"
        else
            @product = "/purchases/err"
        end

    end

    def content_types_provided
        [["application/json", :to_json]]
    end

    def content_types_accepted
        [["application/json", :from_json]]
    end

    def from_json
        puts request.body.to_s
    end

    def to_json
        if request.path_info[:id]

            product = DB[:purchases].find(:_id => BSON::ObjectId(request.path_info[:id])).to_a.first
            product[:id] = product[:_id].to_s
            @product = Purchase.new.extend(PurchaseRepresenter).from_json(product.to_json)

        else
            prod = DB[:purchases].find({:user_id => @user_id}).to_a
            products = []
            prod.each { |product|
                product[:id] = product[:_id].to_s
                products << Purchase.new.extend(PurchaseRepresenter).from_json(product.to_json)
            }
            puts products
            list = Purchases.new.extend(PurchasesRepresenter)
            list.purchases = products
            @product = list

        end
        @product.to_json
    end
end