require_relative '_authController.rb'

class ArticleResource < AuthController
  def allowed_methods
      %w[GET HEAD]
  end



  def resource_exists?
     true
  end

  def allow_missing_post?
    true
  end

  def post_is_create?
    true
  end

  def create_path
    @product = Article.from_attributes(:id => request.path_info[:id])
    @product.to_json
    @product.links[:self]
  end

  def content_types_provided
    [["application/json", :to_json]]
  end

  def content_types_accepted
    [["application/json", :from_json]]
  end

  def from_json

  end


  def is_bought(article)

      puts "article: #{article}"
      puts "user: #{@user_id}"
      results = DB[:purchases].find(:article_id => article, :user_id => @user_id).to_a.first
      if results

          return true
      end
      return false

  end

  def to_json
      if request.path_info[:id]


        product = DB[:articles].find(:_id => BSON::ObjectId(request.path_info[:id])).to_a.first
        product[:id] = product[:_id].to_s
          product[:content_sample] = product[:content][0..140] # just the description
        product[:is_bought] = is_bought(request.path_info[:id])
        @product = Article.new.extend(ArticleRepresenter).from_json(product.to_json)

      else

        prod = DB[:articles].find().to_a

        test = [true, false]
        products = []
        prod.each_with_index { |product, i|
            product[:id] = product[:_id].to_s
            product[:content_sample] = product[:content][0..140] # just the description
            product[:is_bought] = is_bought(product[:id])
            products << Article.new.extend(ArticleRepresenter).from_json(product.to_json)
        }

        puts products
        list = Articles.new.extend(ArticlesRepresenter)
        list.articles = products

        @product = list

      end
      @product.to_json
  end
end