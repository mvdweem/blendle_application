require_relative('_baseController.rb')

#
# All controllers with this controller implemented will required authentication
#

class AuthController < BaseController

    attr_accessor :user_id

    include Webmachine::Resource::Authentication

    def is_authorized?(authorization_header)

        if !request.headers[:authorization]
            return false
        end

        session = DB[:sessions].find({auth_token: request.headers[:authorization]}).to_a.first
        if session
            self.user_id = session[:user_id]
            return true
        end

        fb = HTTP.get(
            "https://graph.facebook.com/me?fields=birthday,email,name,first_name,last_name&access_token=#{request.headers[:authorization]}"
                ).to_s

        user = User.new.extend(UserRepresenter).from_json(fb)

        db_user = DB[:users].find({id: user.id}).to_a.first
        if !db_user
            db_user = DB[:users].insert_one({id: user.id, name: user.name, money: 2500})
        end

        session = DB[:sessions].insert_one({auth_token: request.headers[:authorization], user_id: user.id})

        if session
            self.user_id = user.id
            return true
        end

        self.code = 401
        return false
    end
end