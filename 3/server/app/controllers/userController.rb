require_relative '_authController.rb'
class UserResource < AuthController
    def allowed_methods
        %w[GET HEAD]
    end



    def resource_exists?
        true
    end

    def allow_missing_post?
        true
    end

    def post_is_create?
        true
    end

    def create_path

    end

    def content_types_provided
        [["application/json", :to_json]]
    end

    def content_types_accepted
        [["application/json", :from_json]]
    end

    def from_json

    end


    def to_json

        product = DB[:users].find(:id => @user_id).to_a.first
        @product = User.new.extend(UserRepresenter).from_json(product.to_json)

        @product.to_json
    end
end