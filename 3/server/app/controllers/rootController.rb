require_relative '_baseController.rb'
class RootResource < BaseController
    def allowed_methods
        %w[GET HEAD]
    end



    def resource_exists?
        @product = Root.new.extend(RootRepresenter)
    end

    def allow_missing_post?
        true
    end

    def post_is_create?
        true
    end

    def create_path

    end

    def content_types_provided
        [["application/json", :to_json]]
    end

    def content_types_accepted
        [["application/json", :from_json]]
    end

    def from_json

    end

    def to_json
        @product.to_json
    end
end