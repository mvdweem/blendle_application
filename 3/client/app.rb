require 'sinatra/base'
require 'hyperresource'

App = Sinatra.new

APP_ID     = 304357336335518
APP_SECRET = 'f0084803b4351a7678fdce6138b98d8c'




class App < Sinatra::Base

    require 'koala'
    use Rack::Session::Pool, :expire_after => 3600
    set :views, 'app/views'
    set :port, ENV['PORT']

    before do
        @api = HyperResource.new(
                    root: 'http://readlr-api.herokuapp.com/api',

                    headers: {
                        'Accept' => 'application/json',
                        'Content-Type' => 'application/json',
                        'Authorization' => session[:access_token] || ''
                    },

                )



    end

end





Dir.glob('app/{models,helpers,controllers}/*.rb').each { |file|
    load(file)
}


load('app/routes.rb')



App.run!






