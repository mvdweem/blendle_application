class App
    get '/' do
        erb :index
    end

    before '/' do
        redirect to('/users/login') unless session.has_key?(:access_token)
    end

    get '/article/:id' do
        erb :article, :locals => {:object_id => params[:id]}
    end

    before '/article/:id' do
        redirect to('/users/login') unless session.has_key?(:access_token)
    end

    post '/reaction' do
        status = @api.reactions.post({article_id: params[:id], content: params[:content].gsub(/<\/?[^>]*>/, "")})
        redirect to("/article/#{params[:id]}")
    end

    before '/reaction' do
        redirect to('/users/login') unless session.has_key?(:access_token)
    end

    get '/buy/:id' do
        status = @api.purchases.post({article_id: params[:id]})
        redirect to("/article/#{params[:id]}")
    end

    before '/buy/:id' do
        redirect to('/users/login') unless session.has_key?(:access_token)
    end


    get '/users/login' do
        if !session['access_token']
            erb :login
        end
    end

    get '/login' do
        session['oauth'] = Koala::Facebook::OAuth.new(APP_ID, APP_SECRET, "#{request.base_url}/callback")
        redirect session['oauth'].url_for_oauth_code()
    end

    get '/logout' do
        session['oauth'] = nil
        session['access_token'] = nil
        redirect '/'
    end

    get '/callback' do
        session['access_token'] = session['oauth'].get_access_token(params[:code])
        redirect '/'
    end




end